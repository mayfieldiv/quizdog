@extends('master')

@section('content')
	<div class='row'>
		<div class='col-xs-12'>Welcome, {{ explode(' ', Auth::user()->fullname)[0] }}.</div>
	</div>
	<div class='row'>
		<div class='col-xs-12'><h4><br>Available Quizzes:</h4></div>
	</div>
	<div class='row'>
		<div class='col-md-8'>
			<table class='table'>
				<thead>
					<tr>
					<th>Quiz</th><th>Mode</th><th>Time Expires</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$results = Quiz::whereRaw('close_at > CURRENT_TIMESTAMP and open_at < CURRENT_TIMESTAMP')->get();
					foreach($results as $value){
						$completed = Completed::whereRaw('users_id=? and quizzes_id=?', array(Auth::user()->id, $value->id))->count();
						if($completed == 0){
							echo('<tr><td><a href="quiz/'.$value->id.'">'.$value->title.'</a></td><td>'.($value->mode=='oneshot'?'One-shot':'Guided').'</td><td>'.substr($value->close_at, 0, strlen($value->close_at)-3).'</td></tr>');
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class='row'>
		<div class='col-xs-12'><h4><br>Completed Quizzes:</h4></div>
	</div>
	<div class="row">
		<div class='col-md-8'>
			<table class='table'>
						<thead>
								<tr>
										<th>Quiz</th><th>Time Completed</th><th>Grade</th>
								</tr>
						</thead>
						<tbody>
							<?php
							$completed = Completed::where('users_id', Auth::user()->id)->get();
							foreach($completed as $value){
								$score = Auth::user()->getScore($value->quizzes_id);
								$quiz = Quiz::find($value->quizzes_id);
								echo('<tr><td><a href="results/'.$quiz->id.'/'.Auth::user()->id.'">'.$quiz->title.'</a></td><td>'.substr($value->updated_at, 0, strlen($value->updated_at)-3).'</td><td>'.$score['percentage'].'</td></tr>');
							}
							?>
						</tbody>
			</table>
		</div>
	</div>
@stop
