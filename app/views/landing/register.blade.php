@extends('master')

@section('header')
		<style type="text/css">.form-signin{max-width:330px;margin:0 auto;padding:15px;}.margin-bottom{margin-bottom:10px;}.margin-bottom-sm{margin-bottom:5px;}.form-signin .form-control{position:relative;font-size:14px;height:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:10px;}</style>
@stop

@section('content')
	<form class="form-signin" method="post" action="register">
		<h2 class="form-signin-heading">Join Us!</h2>
		<div class="margin-bottom">
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">
					<i class="fa fa-fw fa-edit"></i>
				</span>
				<input type="text" id="fullname" name="fullname" class="form-control" value={{Input::old('fullname')?Input::old('fullname'): '""'}} placeholder="Full name" autofocus>
			</div>
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">
					<i class="fa fa-user fa-fw"></i>
				</span>
				<input type="text" id="username" name="username" class="form-control" value={{Input::old('username')?Input::old('username'): '""'}} placeholder="Username">
			</div>
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">
					<i class="fa fa-envelope-o fa-fw"></i>
				</span>
				<input type="text" id="email" name="email" class="form-control" value={{Input::old('email')?Input::old('email'): '""'}} placeholder="Email address">
			</div>
			<div class="input-group margin-bottom-sm">
				<span class="input-group-addon">
					<i class="fa fa-key fa-fw"></i>
				</span>
				<input type="password" id="password" name="password" class="form-control" value={{Input::old('password')?Input::old('password'):'""'}} autocomplete='off' placeholder="Password">
			</div>
			{{--<input type="radio" id="admin" name="admin" value="admin" {{Input::old('admin')=='admin'?'checked':''}}> Admin&nbsp;&nbsp;
			<input type="radio" id="taker" name="admin" value="taker" {{Input::old('admin')=='admin'?'':'checked'}}> Taker--}}
		</div> <!-- .margin-bottom -->
		<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
	</form>
@stop

