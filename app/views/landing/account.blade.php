@extends('master')

@section('header')
		<style type="text/css">.form-signin{max-width:330px;margin:0 auto;padding:15px;}.margin-bottom{margin-bottom:10px;}.margin-bottom-sm{margin-bottom:5px;}.form-signin .form-control{position:relative;font-size:14px;height:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:10px;}</style>
@stop

@section('content')
{{ Form::model(Auth::user(), array('class' => 'form-signin')) }}
	<h2 class="form-signin-heading">Edit Account Information</h2>
	<div class="margin-bottom">
		<div class="input-group margin-bottom-sm">
			<span class="input-group-addon">
				<i class="fa fa-fw fa-edit"></i>
			</span>
		{{ Form::text('fullname', Form::getValueAttribute('fullname'), array('class' => 'form-control', 'placeholder' => 'Full Name')) }}
		</div>
		<div class="input-group margin-bottom-sm">
			<span class="input-group-addon">
				<i class="fa fa-user fa-fw"></i>
			</span>
		{{ Form::text('username', Form::getValueAttribute('username'), array('class' => 'form-control', 'placeholder' => 'Username')) }}
		</div>
		<div class="input-group margin-bottom-sm">
			<span class="input-group-addon">
				<i class="fa fa-envelope-o fa-fw"></i>
			</span>
		{{ Form::text('email', Form::getValueAttribute('email'), array('class' => 'form-control', 'placeholder' => 'Email Address')) }}
		</div>
		<div class="input-group margin-bottom-sm">
			<span class="input-group-addon">
				<i class="fa fa-key fa-fw"></i>
			</span>
		{{ Form::password('password', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Password')) }}
		</div>
		<div class="input-group margin-bottom-sm">
			<span class="input-group-addon">
				<i class="fa fa-key fa-fw"></i>
			</span>
		{{ Form::password('password2', array('class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => 'Re-enter password')) }}
		</div>
	</div>
	{{ Form::submit('Save Changes', array('class' => 'btn btn-lg btn-primary btn-block')) }}
	
{{ Form::close() }}
@stop
