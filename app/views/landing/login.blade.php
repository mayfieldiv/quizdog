@extends('master')

@section('header')
	<style type="text/css">.form-signin{max-width:330px;margin:0 auto;padding:15px;}.margin-bottom{margin-bottom:10px;}.margin-bottom-sm{margin-bottom:5px;}.form-signin .form-control{position:relative;font-size:14px;height:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:10px;}</style>
@stop

@section('content')
{{-- 	{{ Form::open(array('action' => 'LandingController@postLogin'), array('class' => 'form-signin')) }}
		{{ Form::label('email', 'Email address') }}
		{{ Form::text('email') }}
		{{ Form::label('password', 'Password') }}
		{{ Form::password('password') }}
	{{ Form::close() }} --}}
      <form class="form-signin" method="post" action="login">
        <h2 class="form-signin-heading">Please sign in</h2>
        <div class="margin-bottom">
         <div class="input-group margin-bottom-sm">
          <span class="input-group-addon">
	    <i class="fa fa-envelope-o"></i>
	  </span>
	  <input type="text" id="email" name="email" class="form-control" placeholder="Email address" value={{Input::old('email')?Input::old('email'):'""'}} autofocus>
	</div>
        <div class="input-group">
          <span class="input-group-addon">
	    <i class="fa fa-key"></i>
	  </span>
	<input type="password" id="password" name="password" class="form-control" placeholder="Password" value={{Input::old('password')?Input::old('password'):'""'}}>
	</div>
	</div> <!-- .margin-bottom -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	<input type="button" value="Register" class="btn btn-lg btn-default btn-block" onClick="window.location.href='/register';">
      </form>
@stop


