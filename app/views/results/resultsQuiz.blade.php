@extends('master')

@section('content')
<ol class="breadcrumb">
  <li><a href="/results">Results</a></li>
  <li class="active">{{ $quiz->title }}</li>
</ol>
<h1>Quiz Report for:</h1>
<h2>{{ $quiz->title }}</h2>
<h5>Available from <strong>{{ substr($quiz->open_at, 0, strlen($quiz->open_at)-3) }}</strong> to <strong>{{ substr($quiz->close_at, 0, strlen($quiz->close_at)-3) }}</strong></h5>
<ul class='nav nav-tabs' id="myTab">
	<li class='active'><a href="#users" data-toggle="tab">Users</a></li>
	<li><a href="#questions" data-toggle="tab">Questions</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade active in" id="users">
		<table class="table table-hover">
			<thead>
				<tr><th><div id='tooltip1' data-toggle="tooltip" data-original-title="Removes respective user's answers for this page's quiz, resets user's score, and allows user to retake if quiz is still available" style="color:blue;cursor:help">Reset</div></th><th>User</th><th># Correct</th><th>Score</th><th>Completed At</th></tr>
			</thead>
			<tbody>
				<form onsubmit="return confirm('Do you really want to reset all user scores for this quiz?');" method='post'>

			<?php
				$completed = Completed::where('quizzes_id', $quiz->id)->get();
				foreach($completed as $value){
					$user = User::find($value->users_id);
					$score = $user->getScore($quiz->id);
					echo('<tr style="cursor:pointer" onclick="window.location=\'/results/'.$quiz->id.'/'.$value->users_id.'\'"><td><button style="padding:0;border:none;background:none;" type="submit" name="submit" value="'.$user->id.'"><i class="fa fa-refresh"/></button></td><td>'.$user->fullname.'</td><td>'.$score['correct'].'/'.$score['total'].'</td><td>'.$score['percentage'].'</td></td><td>'.$value->updated_at.'</td></tr>');
				}
			?>
				</form>
			</tbody>
		</table>
	</div>
	<div class="tab-pane fade" id="questions">
		<table class="table table-striped">
			<thead>
				<tr><th>Question</th><th>Answer Key</th><th># Correct</th><th>Percentage</th></tr>
			</thead>
			<tbody>
			<?php
				$questions = Question::where('quizzes_id', $quiz->id)->get();
				foreach($questions as $question){
					$score = $question->getTotalScore();
					echo('<tr><td>'.$question->question.'</td><td>'.$question->getAnswer().'</td><td>'.$score['correct'].'/'.$score['total'].'</td><td>'.$score['percentage'].'%</td></tr>');
				}
			?>
			</tbody>
		</table>
	</div>
</div>
@stop

@section('script')
<script>
$(document).ready(function(){
	$('#tooltip1').tooltip();
});
</script>
@stop