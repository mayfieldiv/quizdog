@extends('master')

@section('content')
@if (Auth::user()->admin)
	<ol class="breadcrumb">
		<li><a href="/results">Results</a></li>
		<li><a href="/results/{{ $quiz->id }}">{{ $quiz->title }}</a></li>
		<li class="active">{{ $user->fullname }}</li>
	</ol>
@endif
<h1>Detailed Quiz Report:</h1>
<h2>{{ $quiz->title }}</h2>
<h4>Taken by: <strong>{{ $user->fullname }}</strong></h4>
<h5>Score: {{ $user->getScore($quiz->id)['percentage'] }}</h5>
<table class='table table-striped'>
	<thead>
		<tr><th>Question</th><th>Correct Answer</th><th>Answer Given</th><th>Result</th></tr>
	</thead>
	<tbody>
		<?php
			$questions = Question::where('quizzes_id', $quiz->id)->get();
			foreach($questions as $question){
				$user_choice_id = UserAnswer::whereRaw('questions_id=? and users_id=?', array($question->id, $user->id))->pluck('choices_id');
				$user_answer = Choice::find($user_choice_id);
				$user_answer = $user_answer ? $user_answer->choice : '<strong>*No answer given</strong>';
				$correct_answer = Choice::whereRaw('correct=True and questions_id=?', array($question->id))->first();
				echo('<tr><td>'.$question->question.'</td><td>'.$correct_answer->choice.'</td><td>'.$user_answer.'</td><td>'.($user_choice_id==$correct_answer->id?'Correct':'Wrong').'</td></tr>');
			}
		?>
	</tbody>
</table>
@stop
