@extends('master')

@section('content')
<ol class="breadcrumb">
	<li class="active">Results</li>
</ol>
<h1>Results</h1>
	<table class="table table-hover">
		<thead>
			<tr>
				<th><div id='tooltip1' data-toggle="tooltip" data-original-title="Removes all user answers for respective quiz, resets user scores, and allows users to retake if quiz is still available" style="color:blue;cursor:help">Reset</div></th>
				<th>Quiz</th>
				<th>Open Time</th>
				<th>Close Time</th>
				<th>Mode</th>
				<th># takers</th>
			</tr>
		</thead>
		<tbody>
			<form onsubmit="return confirm('Do you really want to reset this user scores for this quiz?');" method='post'>
			<?php
				$quizzes = Quiz::all();
				foreach($quizzes as $quiz){
					$takers = Completed::where('quizzes_id', $quiz->id)->count();
					echo('<tr style="cursor:pointer" onclick="window.location=\'/results/'.$quiz->id.'\'"><td><button style="padding:0;border:none;background:none;" type="submit" name="submit" value="'.$quiz->id.'"><i class="fa fa-refresh"/></button></td><td>'.$quiz->title.'</td><td>'.substr($quiz->open_at, 0, strlen($quiz->open_at)-3).'</td><td>'.substr($quiz->close_at, 0, strlen($quiz->close_at)-3).'</td><td>'.$quiz->mode.'</td><td>'.$takers.'</td></tr>');
				}
			?>
			</form>
		</tbody>
	</table>
@stop

@section('script')
<script>
$(document).ready(function(){
	$('#tooltip1').tooltip();
});
</script>
@stop
