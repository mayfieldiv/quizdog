@extends('master')

@section('content')
<?php
	$quiz = Quiz::find($quizid);
	$questquery = Question::where('quizzes_id', $quizid)->get();
	echo "<div id='quiz'>";
	echo "<h1>".$quiz->title."</h1>";
	$i = 0;
	$questnum = 1;
	echo "<form id='questions' method='post'>";
	
	//iterate through array of questions
	foreach ($questquery as $value) {
		echo "<h4><strong>".$questnum.") </strong>";
		echo $value->question."</h4>";
		$choices = $value->getChoices();
		$choices_array = array();
		foreach($choices as $choice)
			array_push($choices_array, $choice);
		shuffle($choices_array);
		foreach($choices_array as $choice){
			echo Form::radio($value->id, 
							 $choice->encryptedID(),
							 Input::old('$value->id') == $choice->encryptedID(),
							 array('id' => $choice->encryptedID())
							).' ';
			echo Form::label($choice->encryptedID(), $choice->choice).'<br>';
		}
		echo "<br/>";
		
		$questnum++;
	}
	
	echo "<input class='btn btn-primary' type='submit' value='SUBMIT'></input>";
	echo "</form>";
	echo "</div>";
	

?>
@stop
