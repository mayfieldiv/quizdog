<?php
	if(!isset($active)) $active = '';
?>
<!doctype html>
<html>
<head>
	<title>Quiz Dog</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="http://www.ccsmed.com/uploads/images/quiz-icon.png">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootswatch/3.0.0/cosmo/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.0/css/font-awesome.css" rel="stylesheet"> 	
	<style type="text/css">html,body{height:100%;}td,th{text-align:center}#wrap{min-height:100%;margin:0 auto -60px;padding:0 0 60px;}hr{margin:10px 0 10px}#footer{font-size:75%}</style>
	@yield('header')
</head>
<body>
  <div id='wrap'>
	<header class="navbar navbar-default" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">Quiz Dog</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				@if(Auth::check() and Auth::user()->admin)
					<li {{ $active == 'home' ? 'class=active' : '' }}><a href="/">Home</a></li>
					<li {{ $active == 'create' ? 'class=active' : '' }}><a href="/create">Create</a></li>
					<li {{ $active == 'results' ? 'class=active' : '' }}><a href="/results">Results</a></li>
					<li {{ $active == 'manage' ? 'class=active' : '' }}><a href="/manage">Manage</a></li>
					<li><a href="/logout">Logout</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li {{ $active == 'account' ? 'class=active' : '' }}><a href="/account">{{ Auth::user()->fullname; }}</a></li>
				@elseif(Auth::check())
					<li {{ $active == 'home' ? 'class=active' : '' }}><a href="/">Take Quiz</a></li>
					<li><a href="/logout">Logout</a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                        <li {{ $active == 'account' ? 'class=active' : '' }}><a href="/account">{{ Auth::user()->fullname; }}</a></li>
				@else
                    <li {{ $active == 'home' ? 'class=active' : '' }}><a href="/">Take Quiz</a></li>
                    <li {{ $active == 'results' ? 'class=active' : '' }}><a href="/results">Results</a></li>
					<li {{ $active == 'login' ? 'class=active' : '' }}><a href="/login">Login</a></li>
				@endif
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</header>
	@if(Session::has('flash-success'))
		<div class='container'>
                <div class="alert alert-success col-md-offset-2 col-md-8">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Hurray!<br></strong>{{Session::get('flash-success')}}
                </div>
            </div>
	@endif
	@if(Session::has('flash-error'))
            <div class='container'>
                <div class="alert alert-danger col-md-offset-2 col-md-8">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Doh!<br></strong>{{Session::get('flash-error')}}
                </div>
            </div>
	    <?php Session::forget('flash-error'); ?>
        @endif
	@if(Session::has('flash-warning'))
            <div class='container'>
                <div class="alert alert-warning col-md-offset-2 col-md-8">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Uh Oh!<br></strong>{{Session::get('flash-warning')}}
                </div>
            </div>
        @endif
	@if($errors->any())
	    <div class='container'>
	        <div class="alert alert-warning col-md-offset-2 col-md-8">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <strong>Whoops!<br></strong>{{ implode('', $errors->all(':message<br>'))}}
	        </div>
	    </div>
	@endif
	<div class="container">
		@yield('content')
	</div>
  </div>
<div id="footer">
	<div class="container">
		<div class="col-lg-12">
			<p></p>
			<hr>
			&copy; 2013 Sausage Fest
			<p></p>
		</div>
	</div> <!-- /container -->
</div>
	<script src="//code.jquery.com/jquery.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	@yield('script')
</body>
</html>
