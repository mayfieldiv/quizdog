@extends('master')
@section('header')
<style type="text/css">
	input{margin:7px;}button{margin:13px}
</style>
@stop

@section('content')
<ol class="breadcrumb">
	<li><a href="/manage">Manage</a></li>
	<li class="active">{{ $quiz->title }}</li>
</ol>
<h1>{{ $quiz->title }}</h1>
<form method='post'>
	<input type='hidden' name='quiz_id' value={{ $quiz->id }}>
	Title: <input type='text' id='title' name='title' value='{{ $quiz->title }}' data-provide='typeahead' data-items='10' data-source='["{{implode('","', str_replace(array("\"", "'"), array('&#34;', '&#39;'), array_unique(Quiz::all()->lists('title'))))}}"]' autocomplete='off'><br/>
	Start Date and Time: <input type='datetime-local' id='startDateTime' name='startDateTime' value='{{ substr($quiz->open_at, 0, strlen($quiz->open_at)-3) }}'><br>
	End Date and Time: <input type='datetime-local' id='endDateTime' name='endDateTime' value='{{ substr($quiz->close_at, 0, strlen($quiz->close_at)-3) }}'><br>
	Mode: {{ Form::radio('mode', 'oneshot', True, array('id' => 'oneshot')) }}
	<!--Mode: {{ Form::radio('mode', 'oneshot', $quiz->mode=='oneshot', array('id' => 'oneshot')) }}-->
	{{ Form::label('oneshot', 'Oneshot') }}&nbsp;&nbsp;&nbsp;
	<!--{{ Form::radio('mode', 'guided', $quiz->mode=='guided', array('id' => 'guided')) }}-->
	{{ Form::radio('mode', 'guided', False, array('id' => 'guided', 'disabled' => 'disabled')) }}
	{{ Form::label('guided', 'Guided') }}
	<br>
	<button class="btn btn-med btn-primary" type="submit" name='submit' value='Save Changes'>Save Changes</button>
	<button class="btn btn-med btn-info" type="submit" name='submit' value='Edit Questions'>Edit Questions</button>
</form>

@stop

@section('script')
<script>
/* Modernizr 2.7.0 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-inputtypes
 */
;window.Modernizr=function(a,b,c){function u(a){i.cssText=a}function v(a,b){return u(prefixes.join(a+";")+(b||""))}function w(a,b){return typeof a===b}function x(a,b){return!!~(""+a).indexOf(b)}function y(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:w(f,"function")?f.bind(d||b):f}return!1}function z(){e.inputtypes=function(a){for(var d=0,e,g,h,i=a.length;d<i;d++)j.setAttribute("type",g=a[d]),e=j.type!=="text",e&&(j.value=k,j.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(g)&&j.style.WebkitAppearance!==c?(f.appendChild(j),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(j,null).WebkitAppearance!=="textfield"&&j.offsetHeight!==0,f.removeChild(j)):/^(search|tel)$/.test(g)||(/^(url|email)$/.test(g)?e=j.checkValidity&&j.checkValidity()===!1:e=j.value!=k)),n[a[d]]=!!e;return n}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.7.0",e={},f=b.documentElement,g="modernizr",h=b.createElement(g),i=h.style,j=b.createElement("input"),k=":)",l={}.toString,m={},n={},o={},p=[],q=p.slice,r,s={}.hasOwnProperty,t;!w(s,"undefined")&&!w(s.call,"undefined")?t=function(a,b){return s.call(a,b)}:t=function(a,b){return b in a&&w(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=q.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(q.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(q.call(arguments)))};return e});for(var A in m)t(m,A)&&(r=A.toLowerCase(),e[r]=m[A](),p.push((e[r]?"":"no-")+r));return e.input||z(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)t(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof enableClasses!="undefined"&&enableClasses&&(f.className+=" "+(b?"":"no-")+a),e[a]=b}return e},u(""),h=j=null,e._version=d,e}(this,this.document);
$(document).ready(function() {
	if (Modernizr.inputtypes.date) {
		$('#startDateTime').val('{{ str_replace(' ', 'T', substr($quiz->open_at, 0, strlen($quiz->open_at)-3)) }}');
		$('#endDateTime').val('{{ str_replace(' ', 'T', substr($quiz->close_at, 0, strlen($quiz->close_at)-3)) }}');
	}
});
</script>
<script>
//typeahead https://github.com/bassjobsen/Bootstrap-3-Typeahead
!function(e){"use strict";var t=function(t,n){this.$element=e(t);this.options=e.extend({},e.fn.typeahead.defaults,n);this.matcher=this.options.matcher||this.matcher;this.sorter=this.options.sorter||this.sorter;this.autoSelect=typeof this.options.autoSelect=="boolean"?this.options.autoSelect:true;this.highlighter=this.options.highlighter||this.highlighter;this.updater=this.options.updater||this.updater;this.source=this.options.source;this.$menu=e(this.options.menu);this.shown=false;this.listen(),this.showHintOnFocus=typeof this.options.showHintOnFocus=="boolean"?this.options.showHintOnFocus:false};t.prototype={constructor:t,select:function(){var e=this.$menu.find(".active").attr("data-value");if(this.autoSelect||e){this.$element.val(this.updater(e)).change()}return this.hide()},updater:function(e){return e},show:function(){var t=e.extend({},this.$element.position(),{height:this.$element[0].offsetHeight}),n;n=typeof this.options.scrollHeight=="function"?this.options.scrollHeight.call():this.options.scrollHeight;this.$menu.insertAfter(this.$element).css({top:t.top+t.height+n,left:t.left}).show();this.shown=true;return this},hide:function(){this.$menu.hide();this.shown=false;return this},lookup:function(t){var n;this.query=this.$element.val()||"";if(this.query.length<this.options.minLength){return this.shown?this.hide():this}n=e.isFunction(this.source)?this.source(this.query,e.proxy(this.process,this)):this.source;return n?this.process(n):this},process:function(t){var n=this;t=e.grep(t,function(e){return n.matcher(e)});t=this.sorter(t);if(!t.length){return this.shown?this.hide():this}if(this.options.items=="all"||this.options.minLength==0&&!this.$element.val()){return this.render(t).show()}else{return this.render(t.slice(0,this.options.items)).show()}},matcher:function(e){return~e.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(e){var t=[],n=[],r=[],i;while(i=e.shift()){if(!i.toLowerCase().indexOf(this.query.toLowerCase()))t.push(i);else if(~i.indexOf(this.query))n.push(i);else r.push(i)}return t.concat(n,r)},highlighter:function(e){var t=this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");return e.replace(new RegExp("("+t+")","ig"),function(e,t){return"<strong>"+t+"</strong>"})},render:function(t){var n=this;t=e(t).map(function(t,r){t=e(n.options.item).attr("data-value",r);t.find("a").html(n.highlighter(r));return t[0]});if(this.autoSelect){t.first().addClass("active")}this.$menu.html(t);return this},next:function(t){var n=this.$menu.find(".active").removeClass("active"),r=n.next();if(!r.length){r=e(this.$menu.find("li")[0])}r.addClass("active")},prev:function(e){var t=this.$menu.find(".active").removeClass("active"),n=t.prev();if(!n.length){n=this.$menu.find("li").last()}n.addClass("active")},listen:function(){this.$element.on("focus",e.proxy(this.focus,this)).on("blur",e.proxy(this.blur,this)).on("keypress",e.proxy(this.keypress,this)).on("keyup",e.proxy(this.keyup,this));if(this.eventSupported("keydown")){this.$element.on("keydown",e.proxy(this.keydown,this))}this.$menu.on("click",e.proxy(this.click,this)).on("mouseenter","li",e.proxy(this.mouseenter,this)).on("mouseleave","li",e.proxy(this.mouseleave,this))},eventSupported:function(e){var t=e in this.$element;if(!t){this.$element.setAttribute(e,"return;");t=typeof this.$element[e]==="function"}return t},move:function(e){if(!this.shown)return;switch(e.keyCode){case 9:case 13:case 27:e.preventDefault();break;case 38:e.preventDefault();this.prev();break;case 40:e.preventDefault();this.next();break}e.stopPropagation()},keydown:function(t){this.suppressKeyPressRepeat=~e.inArray(t.keyCode,[40,38,9,13,27]);this.move(t)},keypress:function(e){if(this.suppressKeyPressRepeat)return;this.move(e)},keyup:function(e){switch(e.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.lookup()}e.stopPropagation();e.preventDefault()},focus:function(e){this.focused=true;if(this.options.minLength==0&&!this.$element.val()||this.options.matchOnFocus){this.lookup()}},blur:function(e){this.focused=false;if(!this.mousedover&&this.shown)this.hide()},click:function(e){e.stopPropagation();e.preventDefault();this.select();this.$element.focus()},mouseenter:function(t){this.mousedover=true;this.$menu.find(".active").removeClass("active");e(t.currentTarget).addClass("active")},mouseleave:function(e){this.mousedover=false;if(!this.focused&&this.shown)this.hide()}};var n=e.fn.typeahead;e.fn.typeahead=function(n){return this.each(function(){var r=e(this),i=r.data("typeahead"),s=typeof n=="object"&&n;if(!i)r.data("typeahead",i=new t(this,s));if(typeof n=="string")i[n]()})};e.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',minLength:1,scrollHeight:0,autoSelect:true};e.fn.typeahead.Constructor=t;e.fn.typeahead.noConflict=function(){e.fn.typeahead=n;return this};e(document).on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(t){var n=e(this);if(n.data("typeahead"))return;n.typeahead(n.data())})}(window.jQuery)
</script>
@stop
