@extends('master')

@section('content')
	{{ Form::open() }}
	<h2 class="form-signin-heading">Manage User Privileges</h2>
	@foreach($users as $user)
		<p>{{ Form::submit($user->admin ? 'Revoke Admin' : 'Grant Admin', array('name' => $user->id, 'class' => 'btn '.($user->admin ? 'btn-danger' : 'btn-success'))) }} <strong>{{ $user->fullname }}</strong> ({{ $user->username }})</p>
	@endforeach
	{{ Form::close() }}
@stop