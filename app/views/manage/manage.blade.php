@extends('master')

@section('content')
<ol class="breadcrumb">
	<li class="active">Manage</li>
</ol>
<h1>Manage</h1>
<table class="table table-hover">
	<thead>
		<tr>
			<th style="color:red;">Delete</th>
			<th style="color:blue;">Edit</th>
			<th>Quiz</th>
			<th>Open Time</th>
			<th>Close Time</th>
			<th>Mode</th>
			<th># takers</th>
		</tr>
	</thead>
	<tbody>
		<form onsubmit="return confirm('Do you really want to delete this quiz?');" method='post'>
		<?php
		$quizzes = Quiz::all();
		foreach($quizzes as $quiz){
			$takers = array();
			$userAnss = UserAnswer::where('quizzes_id', $quiz->id)->get();
			foreach($userAnss as $userAns){
				array_push($takers, $userAns->users_id);
			}
			$takers = array_unique($takers);
			echo('<tr><td><button style="padding:0;border:none;background:none;" type="submit" name="submit" value="'.$quiz->id.'"><i class="fa fa-lg fa-times"/></button></td><td><a href="/edit/'.$quiz->id.'"><i class="fa fa-edit"></i></a></td><td>'.$quiz->title.'</td><td>'.substr($quiz->open_at, 0, strlen($quiz->open_at)-3).'</td><td>'.substr($quiz->close_at, 0, strlen($quiz->close_at)-3).'</td><td>'.$quiz->mode.'</td><td>'.sizeof($takers).'</td></tr>');
		}
		?>
		</form>
	</tbody>
</table>
<h4><a href='/users'>Manage users instead...</a></h4>
@stop

