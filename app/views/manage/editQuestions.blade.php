@extends('master')

@section('content')
<ol class="breadcrumb">
	<li><a href="/manage">Manage</a></li>
	<li><a href="/edit/{{ $quiz->id }}">{{ $quiz->title }}</a></li>
	<li class="active">Edit Questions</li>
</ol>

<form method=post onsubmit="return confirm('Do you really want to delete this question?');">
	<button type=button class="btn btn-med btn-info pull-right" data-toggle="modal" data-target="#addModal"><i class="fa fa-lg fa-plus"></i> Add Question</button>
	<h1>Edit <strong>{{ $quiz->title }}</strong></h1>
	<table class="table table-hover table-condensed">
		<thead>
			<tr>
				<th style="color:red;">Delete</th>
				<th style="color:blue;">Edit</th>
				<th>Question</th>
				<th>Answer</th>
				<th>Other Choices</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$questions = $quiz->getQuestions();
			foreach($questions as $question){
				echo('<tr>
				<td><button name="delete" value="'.$question->id.'" style="padding:0;border:none;background:none;"><i class="fa fa-lg fa-times"></i></button></td>
				<td><button type=button data-toggle="modal" data-target="#'.$question->id.'" style="padding:0;border:none;background:none;"><i class="fa fa-edit"></i></button></td><td>'.$question->question.'</td>
				<td>'.$question->getAnswer().'</td>
				<td style="text-align:left;"><ul><li>'.implode('</li><li>', Choice::whereRaw('questions_id=? and correct=False', array($question->id))->lists('choice')).'</li></ul></td></tr>');
			}
			?>
		</tbody>
	</table>
</form>
<?php
	$previousQuestions = '"'.implode('","', str_replace(array('"', "'"), array('&#34;', '&#39;'), array_unique(Question::all()->lists('question')))).'"';
	$previousChoices = '"'.implode('","', str_replace(array('"', "'"), array('&#34;', '&#39;'), array_unique(Choice::all()->lists('choice')))).'"';
?>
<form method=post>
	<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModelLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Question</h4>
				</div>
				<div class="modal-body">
					<label for="addquestion">Question</label>
					<input type="text" id="addquestion" class="form-control" name="question" data-provide="typeahead" data-items="10" data-source='[{{ $previousQuestions }}]' autocomplete="off">
					<br>
					<label for="addanswer">Answer</label>
					<input type="text" class="form-control" id="addanswer" name="answer" data-provide="typeahead" data-items="10" data-source='[{{ $previousChoices }}]' autocomplete="off">
					<br>
					<button type=button id='addAnswer' data-addcount=1 class="btn btn-default">Add Choice</button>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button name="add" value=1 class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<?php
foreach($questions as $question){
	$choices = '';
	foreach(Choice::whereRaw('questions_id=? and correct=False', array($question->id))->get() as $choice){
		$choices = $choices.'<div class="edit" id="id'.$choice->id.'"><label for="'.$question->id.'editchoice'.$choice->id.'"><button class="removeButton" id="editremoveButton'.$choice->id.'" data-question="'.$question->id.'" data-choice="'.$choice->id.'" type=button style="padding:0;border:none;background:none;"><i class="fa fa-lg fa-times"></i></button> Alternative Choice</label><input type="text" class="form-control" id="'.$question->id.'editchoice'.$choice->id.'" name="'.$choice->id.'" value="'.$choice->choice.'" data-provide="typeahead" data-items="10" data-source=\'['.$previousChoices.']\' autocomplete="off"><br></div>
		';
	}
	echo('<form method=post>
	<div class="modal fade" id="'.$question->id.'" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit Question</h4>
				</div>
				<div class="modal-body">
					<label for="question'.$question->id.'">Question</label>
					<input type="text" id="question'.$question->id.'" class="form-control" name="question" value="'.$question->question.'" data-provide="typeahead" data-items="10" data-source=\'['.$previousQuestions.']\' autocomplete="off">
					<br>
					<label for="answer'.$question->id.'">Answer</label>
					<input type=hidden name="answerID" value="'.$question->getAnswerID().'">
					<input type="text" class="form-control" id="answer'.$question->id.'" name="answer" value="'.$question->getAnswer().'" data-provide="typeahead" data-items="10" data-source=\'['.$previousChoices.']\' autocomplete="off">
					<br>
					'.$choices.'
					<button type=button class="editAnswer btn btn-default" data-question="'.$question->id.'" data-count=1 class="btn btn-default">Add Choice</button>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button name="edit" value="'.$question->id.'" class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>');
}
?>
@stop

@section('script')
<script>
function newAnswer(count){
	return '<div class="add" id="'+count+'"><label for="addchoice'+count+'"><button id="addremoveButton'+count+'" data-choice="'+count+'" type=button style="padding:0;border:none;background:none;"><i class="fa fa-lg fa-times"></i></button> Alternative Choice</label><input type="text" class="form-control" id="addchoice'+count+'" name="choice'+count+'" data-provide="typeahead" data-items="10" data-source=\'[{{ $previousChoices }}]\' autocomplete="off"><br></div>';
}
$(document).ready( function(){
	$('#addAnswer').click( function(){
		count = $('#addAnswer').data('addcount') + 1;
		$(this).data('addcount', count);
		$(this).before(newAnswer($(this).data('addcount')));
		$('#addremoveButton'+count).click(function(){
			$('div.add#'+$(this).data('choice')).remove();
		});
	});
	$('.editAnswer').click(function(){
		count = $(this).data('count') + 1;
		$(this).data('count', count);
		questionid = $(this).data('question');
		$(this).before('<div class="edit" id="'+questionid+'id-'+count+'"><label for="'+questionid+'editchoice-'+count+'"><button data-count="'+count+'" id="'+questionid+'editremoveButton-'+count+'" type=button style="padding:0;border:none;background:none;"><i class="fa fa-lg fa-times"></i></button> Alternative Choice</label><input type="text" id="'+questionid+'editchoice-'+count+'" class="form-control" name="-'+count+'" data-provide="typeahead" data-items="10" data-source=\'[{{ $previousChoices }}]\' autocomplete="off"><br></div>');
		console.log('if '+'#'+questionid+'editremoveButton-'+count+' is clicked: remove div.edit#'+questionid+'id-'+count);
		$('#'+questionid+'editremoveButton-'+count).click(function(){
			$('div.edit#'+questionid+'id-'+$(this).data('count')).remove();
			console.log('#'+questionid+'editremoveButton-'+$(this).data('count')+' was clicked: div.edit#'+questionid+'id-'+$(this).data('count')+' was removed');
		});
	});
	$('.removeButton').click(function(){
		$('div.edit#id'+$(this).data('choice')).remove();
	});
});
</script>
<script>
//typeahead https://github.com/bassjobsen/Bootstrap-3-Typeahead
!function(e){"use strict";var t=function(t,n){this.$element=e(t);this.options=e.extend({},e.fn.typeahead.defaults,n);this.matcher=this.options.matcher||this.matcher;this.sorter=this.options.sorter||this.sorter;this.autoSelect=typeof this.options.autoSelect=="boolean"?this.options.autoSelect:true;this.highlighter=this.options.highlighter||this.highlighter;this.updater=this.options.updater||this.updater;this.source=this.options.source;this.$menu=e(this.options.menu);this.shown=false;this.listen(),this.showHintOnFocus=typeof this.options.showHintOnFocus=="boolean"?this.options.showHintOnFocus:false};t.prototype={constructor:t,select:function(){var e=this.$menu.find(".active").attr("data-value");if(this.autoSelect||e){this.$element.val(this.updater(e)).change()}return this.hide()},updater:function(e){return e},show:function(){var t=e.extend({},this.$element.position(),{height:this.$element[0].offsetHeight}),n;n=typeof this.options.scrollHeight=="function"?this.options.scrollHeight.call():this.options.scrollHeight;this.$menu.insertAfter(this.$element).css({top:t.top+t.height+n,left:t.left}).show();this.shown=true;return this},hide:function(){this.$menu.hide();this.shown=false;return this},lookup:function(t){var n;this.query=this.$element.val()||"";if(this.query.length<this.options.minLength){return this.shown?this.hide():this}n=e.isFunction(this.source)?this.source(this.query,e.proxy(this.process,this)):this.source;return n?this.process(n):this},process:function(t){var n=this;t=e.grep(t,function(e){return n.matcher(e)});t=this.sorter(t);if(!t.length){return this.shown?this.hide():this}if(this.options.items=="all"||this.options.minLength==0&&!this.$element.val()){return this.render(t).show()}else{return this.render(t.slice(0,this.options.items)).show()}},matcher:function(e){return~e.toLowerCase().indexOf(this.query.toLowerCase())},sorter:function(e){var t=[],n=[],r=[],i;while(i=e.shift()){if(!i.toLowerCase().indexOf(this.query.toLowerCase()))t.push(i);else if(~i.indexOf(this.query))n.push(i);else r.push(i)}return t.concat(n,r)},highlighter:function(e){var t=this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&");return e.replace(new RegExp("("+t+")","ig"),function(e,t){return"<strong>"+t+"</strong>"})},render:function(t){var n=this;t=e(t).map(function(t,r){t=e(n.options.item).attr("data-value",r);t.find("a").html(n.highlighter(r));return t[0]});if(this.autoSelect){t.first().addClass("active")}this.$menu.html(t);return this},next:function(t){var n=this.$menu.find(".active").removeClass("active"),r=n.next();if(!r.length){r=e(this.$menu.find("li")[0])}r.addClass("active")},prev:function(e){var t=this.$menu.find(".active").removeClass("active"),n=t.prev();if(!n.length){n=this.$menu.find("li").last()}n.addClass("active")},listen:function(){this.$element.on("focus",e.proxy(this.focus,this)).on("blur",e.proxy(this.blur,this)).on("keypress",e.proxy(this.keypress,this)).on("keyup",e.proxy(this.keyup,this));if(this.eventSupported("keydown")){this.$element.on("keydown",e.proxy(this.keydown,this))}this.$menu.on("click",e.proxy(this.click,this)).on("mouseenter","li",e.proxy(this.mouseenter,this)).on("mouseleave","li",e.proxy(this.mouseleave,this))},eventSupported:function(e){var t=e in this.$element;if(!t){this.$element.setAttribute(e,"return;");t=typeof this.$element[e]==="function"}return t},move:function(e){if(!this.shown)return;switch(e.keyCode){case 9:case 13:case 27:e.preventDefault();break;case 38:e.preventDefault();this.prev();break;case 40:e.preventDefault();this.next();break}e.stopPropagation()},keydown:function(t){this.suppressKeyPressRepeat=~e.inArray(t.keyCode,[40,38,9,13,27]);this.move(t)},keypress:function(e){if(this.suppressKeyPressRepeat)return;this.move(e)},keyup:function(e){switch(e.keyCode){case 40:case 38:case 16:case 17:case 18:break;case 9:case 13:if(!this.shown)return;this.select();break;case 27:if(!this.shown)return;this.hide();break;default:this.lookup()}e.stopPropagation();e.preventDefault()},focus:function(e){this.focused=true;if(this.options.minLength==0&&!this.$element.val()||this.options.matchOnFocus){this.lookup()}},blur:function(e){this.focused=false;if(!this.mousedover&&this.shown)this.hide()},click:function(e){e.stopPropagation();e.preventDefault();this.select();this.$element.focus()},mouseenter:function(t){this.mousedover=true;this.$menu.find(".active").removeClass("active");e(t.currentTarget).addClass("active")},mouseleave:function(e){this.mousedover=false;if(!this.focused&&this.shown)this.hide()}};var n=e.fn.typeahead;e.fn.typeahead=function(n){return this.each(function(){var r=e(this),i=r.data("typeahead"),s=typeof n=="object"&&n;if(!i)r.data("typeahead",i=new t(this,s));if(typeof n=="string")i[n]()})};e.fn.typeahead.defaults={source:[],items:8,menu:'<ul class="typeahead dropdown-menu"></ul>',item:'<li><a href="#"></a></li>',minLength:1,scrollHeight:0,autoSelect:true};e.fn.typeahead.Constructor=t;e.fn.typeahead.noConflict=function(){e.fn.typeahead=n;return this};e(document).on("focus.typeahead.data-api",'[data-provide="typeahead"]',function(t){var n=e(this);if(n.data("typeahead"))return;n.typeahead(n.data())})}(window.jQuery)
</script>
@stop