<?php
class UserAnswer extends Eloquent {
	
	protected $table = 'user_answers';
	protected $fillable = array('choices_id', 'questions_id', 'quizzes_id', 'users_id');
	
}
