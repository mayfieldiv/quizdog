<?php
class Choice extends Eloquent {

	protected $fillable = array('choice', 'correct', 'questions_id');
	
	public function encryptedID(){
		$key = "Sausage Fest";
		$encrypted_id = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), strval($this->id), MCRYPT_MODE_CBC, md5(md5($key))));
		return BIN2HEX($encrypted_id);
		
	}
}
