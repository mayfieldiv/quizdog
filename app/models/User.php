<?php

use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

	protected $table = 'users';

	protected $hidden = array('password');
	protected $fillable = array('username', 'fullname', 'email', 'password');

	public function getScore($quizid) {
		$answers = UserAnswer::whereRaw('users_id=? and quizzes_id=?', array($this->id, $quizid))->get();
		$numquestions = Question::where('quizzes_id', $quizid)->count();
		$correct = 0;
		foreach($answers as $user_answer)
			if( Question::find($user_answer->questions_id)->isCorrectID($user_answer->choices_id) )
				$correct += 1;
		$percentage = $correct/max(1, $numquestions)*100;
		if($percentage >= 93) $letter = 'A';
		elseif($percentage >= 90) $letter = 'A-';
		elseif($percentage >= 87) $letter = 'B+';
		elseif($percentage >= 83) $letter = 'B';
		elseif($percentage >= 80) $letter = 'B-';
		elseif($percentage >= 77) $letter = 'C+';
		elseif($percentage >= 73) $letter = 'C';
		elseif($percentage >= 70) $letter = 'C-';
		elseif($percentage >= 60) $letter = 'D';
		else $letter = 'F';
		return array('correct'=>$correct, 'total'=>$numquestions, 'percentage'=>number_format($percentage, 2).'% ('.$letter.')');
	}
	
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

}
