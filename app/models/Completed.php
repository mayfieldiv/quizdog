<?php
class Completed extends Eloquent {

	protected $table = 'completed';
	protected $fillable = array('quizzes_id', 'users_id');
	
}
