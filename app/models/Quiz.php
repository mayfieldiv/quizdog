<?php
class Quiz extends Eloquent {

	protected $fillable = array('title', 'mode', 'open_at', 'close_at');
	
	public function decryptedID($encrypted_id){
		$key = "Sausage Fest";
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode(hex2bin($encrypted_id)), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return intval($decrypted);
	}
	public function getQuestions(){
		return Question::where('quizzes_id', $this->id)->get();
	}
}
