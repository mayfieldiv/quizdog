<?php
class Question extends Eloquent {
	protected $fillable = array('question', 'quizzes_id');
	
	public function getAnswer(){
		return Choice::whereRaw('questions_id=? and correct=True', array($this->id))->pluck('choice');
	}
	public function getAnswerID(){
		return Choice::whereRaw('questions_id=? and correct=True', array($this->id))->pluck('id');
	}
	public function isCorrect($answer){
		return $this->getAnswer() == $answer;
	}
	public function isCorrectID($choiceID){
		return $this->getAnswerID() == $choiceID;
	}
	public function addChoices($choices){
		Choice::create(array(
			'choice'=>array_shift($choices),
			'correct'=>True,
			'questions_id'=>$this->id
		));
		foreach($choices as $choice){
			Choice::create(array(
				'choice'=>$choice,
				'correct'=>False,
				'questions_id'=>$this->id
			));
		}
	}
	public function getChoices(){
		return Choice::where('questions_id', $this->id)->get();
	}
	public function getTotalScore(){
		$user_answers = UserAnswer::where('questions_id', $this->id)->get();
		$correct = 0;
		foreach($user_answers as $user_answer)
			if($this->isCorrectID($user_answer->choices_id))
				$correct += 1;
		return array('correct'=>$correct, 'total'=>sizeof($user_answers), 'percentage'=>number_format($correct/max(1, sizeof($user_answers))*100, 2));
	}
}
