<?php

class SampleQuizSeeder extends Seeder {

    public function run()
    {
        DB::table('quizzes')->delete();
		
// Linux
        $quiz = Quiz::create(array('title'=>'Linux', 'mode'=>'oneshot', 'open_at'=>'2013-11-20 12:30:00', 'close_at'=>'2013-12-25 23:55:00'));
		$question = Question::create(array('question'=>'What command is used to count the total number of lines, words, and characters contained in a file?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('wc', 'wcount', 'countw', 'count p', 'None of the above'));
		$question = Question::create(array('question'=>'What command is used to remove files?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('rm', 'dm', 'delete', 'erase', 'None of the above'));
		$question = Question::create(array('question'=>'What command is used to remove the directory?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('rmdir', 'remove', 'rd', 'rdir', 'None of the above'));
		$question = Question::create(array('question'=>'What command is used with vi editor to delete a single character?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('x', 'y', 'a', 'z', 'None of the above'));
		$question = Question::create(array('question'=>'What hardware architectures are not supported by Red Hat?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('Macintosh', 'IBM-compatible', 'Alpha', 'SPARC', 'None of the above'));
		$question = Question::create(array('question'=>'The physical layer of a network', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('defines the electrical characteristics of signals passed between the computer and communication devices', 'controls error detection and correction', 'constructs packets of data and sends them across the network', 'All of the above', 'None of the above'));
		$question = Question::create(array('question'=>'What TCP/IP protocol is used for remote terminal connection service?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('TELNET', 'RARP', 'FTP', 'UDP', 'None of the above'));
		$question = Question::create(array('question'=>'How many networks and nodes per network, are allowed by the Class B network?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('16,384 networks and 65,534 nodes per network', '127 networks and 16,777,216 nodes per network', '2,097,152 networks and 254 nodes per network', 'All of the above', 'None of the above'));
		$question = Question::create(array('question'=>'What service is used to translate domain names to IP addresses?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('DNS', 'SMB', 'NIS', 'NFS', 'None of the above'));
		$question = Question::create(array('question'=>'Which of the following command is used to create a Linux installation hoot floppy?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('dd and rawrite', 'bootfp disk', 'ww and rawwrite', 'mkboot disk', 'None of the above'));
		
// Linux 2
		$quiz = Quiz::create(array('title'=>'Linux 2', 'mode'=>'oneshot', 'open_at'=>'2013-11-15 01:00:00', 'close_at'=>'2013-12-18 23:00:00'));
		$question = Question::create(array('question'=>'How can you add Amit, a new user, to your system?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('All of the above', 'Using adduser', 'Using linuxconf', 'Using useradd', 'None of the above'));
		$question = Question::create(array('question'=>'What file specifies the order in which to use specified name services?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('/etc/nsswitch.conf', '/etc/nsorder', '/etc/services', '/etc/hosts', 'None of the above'));
		$question = Question::create(array('question'=>'How many primary partitions can exist on one drive?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('4', '16', '2', '1', 'None of the above'));
		$question = Question::create(array('question'=>'In which directory can you store system user default files used for creating user directories?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('/etc/skel', '/etc/default', '/usr/tmp', '/etc/users', 'None of the above'));
		$question = Question::create(array('question'=>'How could you install the file ipchains-1.3.9-5.i386.rpm?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('rpm -i ipchains* .i386.rpm', 'rpm -Uvh ipchains', 'rpm -qip ipchains', 'rpm -e ipchains-1.3.9-5.i386.rpm', 'None of the above'));
		$question = Question::create(array('question'=>'What does FSF stand for?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('Free Software Foundation', 'File Server First', 'First Serve First', 'Free Software File', 'None of the above'));
		$question = Question::create(array('question'=>'Which of the following is a valid format for mounting a CD-ROM drive?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('All of the above', 'mount /dev/cdrom', 'mount /mnt/cdrom', 'mount -t iso9660 /dev/cdrom / mnt/cdrom', 'None of the above'));
		$question = Question::create(array('question'=>'What command do you use to create Linux file systems?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('mkfs', 'fdisk', 'fsck', 'mount', 'None of the above'));
		$question = Question::create(array('question'=>'Which of the following command can you execute to count the number of lines in a file?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('wc - l', 'lc', 'cl', 'count', 'None of the above'));
		$question = Question::create(array('question'=>'Which of the following is not a communication command?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('grep', 'mail', 'mesg', 'write', 'None of the above'));
		
// Hey Chis
		$quiz = Quiz::create(array('title'=>'Hey Chis', 'mode'=>'oneshot', 'open_at'=>'2013-11-26 03:00:00', 'close_at'=>'2013-12-28 23:30:00'));
		$question = Question::create(array('question'=>'Is you Chis?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('I Chis alrite', 'Mike Sheffey!'));
		$question = Question::create(array('question'=>'Is Dr. Sykes bald?', 'quizzes_id'=>$quiz->id));
		$question->addChoices(array('Not completely', 'Don\'t insult his luscious locks', 'Total cue-ball'));

	}
	
}
