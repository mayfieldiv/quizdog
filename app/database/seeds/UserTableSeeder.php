<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create(array(
            'email' => 'reynoldsbm@email.wofford.edu',
	    'fullname' => 'Mayfield Reynolds',
	    'username' => 'BMayfieldR',
            'password' => Hash::make('budgetDiscord'),
	    'admin' => true
        ));
	
	User::create(array(
	    'email' => 'novakcm@email.wofford.edu',
	    'fullname' => 'Chris Novak',
	    'username' => 'ChrisMN',
	    'password' => Hash::make('kavon'),
	    'admin' => true
	));

	User::create(array(
	    'email' => 'sheffeymj@email.wofford.edu',
	    'fullname' => 'Mike Sheffey',
	    'username' => 'MikeJS',
	    'password' => Hash::make('yeffehs'),
	    'admin' => true
	));

	User::create(array(
	    'email' => 'mcafeeca@email.wofford.edu',
	    'fullname' => 'Allen McAfee',
	    'username' => 'CAllenM',
	    'password' => Hash::make('eefacm'),
	    'admin' => true
	));

	User::create(array(
	    'email' => 'mcphearsonll@email.wofford.edu',
	    'fullname' => 'Lamont McPhearson',
	    'username' => 'LamontLM',
	    'password' => Hash::make('nosraehpcm'),
	    'admin' => true
	));

	User::create(array(
	    'email' => 'millertj@email.wofford.edu',
	    'fullname' => 'Thomas Miller',
	    'username' => 'ThomasJM',
	    'password' => Hash::make('rellim'),
	    'admin' => true
	));
    }
}
