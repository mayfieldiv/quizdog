<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_answers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('choices_id')->unsigned();
			$table->integer('questions_id')->unsigned();
            $table->integer('quizzes_id')->unsigned();
			$table->integer('users_id')->unsigned();
			$table->timestamps();
			$table->foreign('questions_id')->references('id')->on('questions');
            $table->foreign('quizzes_id')->references('id')->on('quizzes');
			$table->foreign('users_id')->references('id')->on('users');
			$table->foreign('choices_id')->references('id')->on('choices');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_answers');
	}

}
