<?php

class CreateController extends BaseController {
	public function __construct() {
		$this->beforeFilter('admin');
	}
	public function getCreate() {
		return View::make('create.createQuiz')->with('active', 'create');
	}
	public function postCreate() {
		$input = Input::all();
		$input['startDateTime'] = str_replace('T', ' ', $input['startDateTime']);
		$input['endDateTime'] = str_replace('T', ' ', $input['endDateTime']);
		$rules = array('title' => 'required|unique:quizzes,title', 'startDateTime' => 'required|date_format:Y-m-d G:i', 'endDateTime' => 'required|date_format:Y-m-d G:i', 'mode' => 'required');
		$v = Validator::make($input, $rules);
		if($v->fails()){
			return Redirect::route('create')->withInput()->withErrors($v);
		}
		Quiz::create(array(
			'title' => $input['title'],
			'open_at' => $input['startDateTime'],
			'close_at' => $input['endDateTime'],
			'mode' => $input['mode']
		));
		return Redirect::route('addQuestions')->with('title', Input::get('title'));
	}
	public function getQuestions() {
		if(Session::has('title'))
			return View::make('create.addQuestions')->with('active', 'create');
		return Redirect::route('landing')->with('flash-warning', 'An error has occurred.');
	}
	public function postQuestions() {
		$input = Input::all();
		$id = Quiz::where('title', $input['ztitle'])->pluck('id');
		krsort($input);
		$questions = [];
		foreach ($input as $key => $value) {
			if (strpos($key, 'txtquestion') === 0 ) {
				$question = Question::create(array(
					'question' => $value,
					'quizzes_id' => $id
				));
				$questions[substr($key, 11)] = $question;
			} elseif (strpos($key, 'answer') === strlen($key)-6) {
				Choice::create(array(
					'choice' => $value,
					'correct' => True,
					'questions_id' => $questions[substr($key, 1, strlen($key)-7)]->id
				));
			} elseif (strpos($key, 'Answer') === strlen($key)-7) {
				Choice::create(array(
					'choice' => $value,
					'correct' => False,
					'questions_id' => $questions[substr($key, 1, strlen($key)-8)]->id
				));
			}
		}
		return Redirect::route('landing')->with('flash-success','Questions have been modified for quiz "'.$input['ztitle'].'"'); 
	}
}