<?php

class ManageController extends BaseController {
	public function __construct(){
		$this->beforeFilter('admin');
	}
	public function getManage(){
		return View::make('manage.manage')->with('active', 'manage');
	}
	public function postManage(){
		$input = Input::all();
		$quiz = Quiz::find($input['submit']);
		UserAnswer::where('quizzes_id', $quiz->id)->delete();
		$questions = Question::where('quizzes_id', $quiz->id)->get();
		foreach($questions as $question){
			Choice::where('questions_id', $question->id)->delete();
			$question->delete();
		}
		$title = $quiz->title;
		Completed::where('quizzes_id', $quiz->id)->delete();
		$quiz->delete();
		return Redirect::route('manage')->with('flash-success', $title.' was successfully removed from the database.');
	}
	public function getEditQuiz(Quiz $quiz){
		return View::make('manage.editQuiz')->with('quiz', $quiz);
	}
	public function postEditQuiz(Quiz $quiz){
		$input = Input::all();
		$input['startDateTime'] = str_replace('T', ' ', $input['startDateTime']);
		$input['endDateTime'] = str_replace('T', ' ', $input['endDateTime']);
		$rules = array('title' => 'required'.($input['title']==$quiz->title ? '' : '|unique:quizzes,title'), 'startDateTime' => 'required|date_format:Y-m-d G:i', 'endDateTime' => 'required|date_format:Y-m-d G:i', 'mode' => 'required');
		$v = Validator::make($input, $rules);
		if($v->fails()){
			return Redirect::to('edit/'.$quiz->id)->withInput()->withErrors($v);
		}
		
		$quiz->title = $input['title'];
		$quiz->mode = $input['mode'];
		$quiz->open_at = $input['startDateTime'];
		$quiz->close_at = $input['endDateTime'];
		$quiz->save();

		if($input['submit']=="Edit Questions")
			return Redirect::to('editquestions/'.$quiz->id);
		return Redirect::to('edit/'.$quiz->id)->with('flash-success', "Your changes to ".$input['title']." were saved successfully.");
	}
	public function getEditQuestions(Quiz $quiz){
		return View::make('manage.editQuestions')->with('quiz', $quiz);
	}
	public function postEditQuestions(Quiz $quiz){
		$input = Input::all();
		if(Input::has('delete')){
			$question = Question::find($input['delete']);
			UserAnswer::where('questions_id', $question->id)->delete();
			Choice::where('questions_id', $question->id)->delete();
			$q = $question->question;
			$question->delete();
			return Redirect::to('editquestions/'.$quiz->id)->with('flash-success', '"'.$q.'" was successfully removed from '.$quiz->title.'.');
		} elseif(Input::has('edit')){
			$questionid = $input['edit'];
			$question = Question::find($questionid);
			$question->question = $input['question'];
			$question->save();
			$answer = Choice::find($input['answerID']);
			$answer->choice = $input['answer'];
			$answer->save();
			unset($input['answer']);
			unset($input['answerID']);
			unset($input['edit']);
			unset($input['question']);
			$prevchoices = Choice::whereRaw('questions_id=? and correct=False', array($questionid))->lists('id');
			$deleted = array();
			foreach($prevchoices as $prevchoice)
				if(!in_array($prevchoice, array_keys($input)))
					array_push($deleted, $prevchoice);
			foreach($deleted as $choiceid){
				UserAnswer::where('choices_id', $choiceid)->delete();
				Choice::destroy($choiceid);
			}
			foreach($input as $key=>$value){
				if($key > 0){
					Choice::find($key)->update(array('choice' => $value));
				} else {
					Choice::create(array(
						'choice' => $value,
						'correct' => FALSE,
						'questions_id' => $questionid
					));
				}
			}
	        return Redirect::to('editquestions/'.$quiz->id)->with('flash-success', '"'.$question->question.'" was successfully edited.');
		} elseif(Input::has('add')) {
			$choices = array($input['answer']);
			foreach($input as $key => $value)
				if(strpos($key, 'choice') === 0)
					array_push($choices, $value);
			$question = Question::create(array('question'=>$input['question'], 'quizzes_id'=>$quiz->id));
			$question->addChoices($choices);
			return Redirect::to('editquestions/'.$quiz->id)->with('flash-success', '"'.$question->question.'" was successfully added to '.$quiz->title.'.');
		}
	}
	public function getUsers(){
		$users = User::all();
		return View::make('manage.users')->with('users', $users);
	}
	public function postUsers(){
		$input = array_keys(Input::all());
		$user = User::find($input[1]);
		$admin = $user->admin;
		$user->admin = 1 - $admin;
		$user->save();
		if($admin==0)
			return Redirect::route('users')->with('flash-success', $user->fullname.' was granted admin privileges');
		else {
			return Redirect::route('users')->with('flash-success', 'Admin privileges were revoked from '.$user->fullname);
		}
	}
}