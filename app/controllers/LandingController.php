<?php
	
class LandingController extends BaseController {
	public function __construct(){
		$this->beforeFilter('auth', array('only' => array('getIndex', 'getAccount', 'postAccount')));
		$this->beforeFilter('guest', array('except' => array('getIndex', 'getLogout', 'getAccount', 'postAccount')));
		//$this->beforeFilter('csrf', array('on' => 'post'));
	}
	public function getIndex(){
		return View::make('landing.home')->with('active', 'home');
	}
	public function getLogin(){
		return View::make('landing.login')->with('active', 'login');
	}
	public function postLogin(){
		if(Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'))))
			return Redirect::intended('/');
		else
			return Redirect::route('login')->withErrors(array('Incorrect email/password. Please try again.'))->withInput();
	}
	public function getLogout(){
		Auth::logout();
		return Redirect::route('login');
	}
	public function getRegister(){
		return View::make('landing.register');
	}
	public function postRegister(){
		$input = Input::all();
		$rules = array(
			'email' => 'required|min:4|max:32|email|unique:users,email', 
			'password' => 'required', 
			'username' => 'required|unique:users,username', 
			'fullname' => 'required'
		);
		$v = Validator::make($input, $rules);
		if ($v->fails())
		{
			return Redirect::route('register')->withErrors($v)->withInput();
		}
		else
		{
			User::create(array(
				'fullname' => $input['fullname'], 
				'username' => $input['username'], 
				'email' => $input['email'], 
				'password' => Hash::make($input['password']), 
				'admin' => False
			));
			Auth::attempt(array(
				'email' => Input::get('email'), 
				'password' => Input::get('password')
			));
			return Redirect::route('landing')->with('flash-success', 'New user has been created');
		}
	}
	public function getAccount(){
		return View::make('landing.account')->with('active', 'account');
	}
	public function postAccount(){
		$user = Auth::user();
		$input = Input::all();
		$rules = array(
			'email' => 'required|min:4|max:32|email'.($input['email']==$user->email?'':'|unique:users,email'), 
			'username' => 'required'.($input['username']==$user->username?'':'|unique:users,username'), 
			'fullname' => 'required'
		);
		$v = Validator::make($input, $rules);
		if ($v->fails())
		{
			return Redirect::route('account')->withErrors($v)->withInput();	
		}
		else {
			$message = 'Account has been successfully modified.';
			$user->fullname = $input['fullname'];
			$user->username = $input['username'];
			$user->email = $input['email'];
			if($input['password']!=''){
				if($input['password']==$input['password2'])
					$user->password = Hash::make($input['password']);
				else
					return Redirect::route('account')->with('flash-warning', 'Passwords do not match.');
			}
			$user->save();
			return Redirect::route('account')->with('flash-success', $message);
		}
	}
}