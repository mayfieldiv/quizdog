<?php

class ResultsController extends BaseController {
	public function __construct(){
		$this->beforeFilter('admin', array('except' => 'getResultsUser'));
	}
	public function getResults(){
		return View::make('results.results')->with('active', 'results');
	}
	public function postResults(){
		$input = Input::all();
		$quiz = Quiz::find($input['submit']);
		UserAnswer::where('quizzes_id', $quiz->id)->delete();
		Completed::where('quizzes_id', $quiz->id)->delete();
		return Redirect::route('results')->with('flash-success', 'Scores have been reset for "'.$quiz->title.'"');
	}
	public function getResultsQuiz(Quiz $quiz){
		return View::make('results.resultsQuiz')->with('active', 'results')->with('quiz', $quiz);
	}
	public function postResultsQuiz(Quiz $quiz){
		$input = Input::all();
		$user = User::find($input['submit']);
		UserAnswer::whereRaw('quizzes_id=? and users_id=?', array($quiz->id, $user->id))->delete();
		Completed::whereRaw('quizzes_id=? and users_id=?', array($quiz->id, $user->id))->delete();
		return Redirect::to('results/'.$quiz->id)->with('flash-success', 'Scores have been reset for "'.$user->fullname.'"');
	}
	public function getResultsUser(Quiz $quiz, User $user){
		if(Auth::guest() or (!Auth::user()->admin and Auth::user()->id != $user->id))
			return App::abort(404);
		if(Completed::whereRaw('users_id=? and quizzes_id=?', array($user->id, $quiz->id))->count() == 0)
			return App::abort(404);
		return View::make('results.resultsUser')->with('active', 'results')->with('quiz', $quiz)->with('user', $user);
	}
}