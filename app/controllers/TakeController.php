<?php

class TakeController extends BaseController {
	public function __construct(){
		$this->beforeFilter('auth');
	}
	public function getQuiz(Quiz $quiz){
		if(!Auth::user()->admin and (strtotime($quiz->close_at) < time() or strtotime($quiz->open_at) > time()))
			return Redirect::route('landing')->with('flash-warning', 'This quiz is not currently available.');
		if(Completed::whereRaw('users_id=? and quizzes_id=?', array(Auth::user()->id, $quiz->id))->count() == 0)
			return View::make('take.takeQuiz')->with('quizid',$quiz->id);
		return Redirect::route('landing')->with('flash-warning', 'You\'ve already taken this quiz.');
	}
	public function postQuiz(Quiz $quiz){
		$input = Input::all();
		$rules = array();
		foreach ($quiz->getQuestions() as $question)
			$rules[strval($question->id)] = 'required';
		$v = Validator::make($input, $rules);
		if ($v->fails()){
			$out = array('<br><strong>The following questions must be answered:</strong>');
			foreach ($v->errors()->all() as $error){
				array_push($out, Question::find(explode(' ', $error)[1])->question);
			}
			return Redirect::to('quiz/'.$quiz->id)->withErrors($out)->withInput();
		}
		foreach ($input as $question_id => $encrypted_choice_id ) {
			UserAnswer::create(array(
				'questions_id' => $question_id, 
				'choices_id' => $quiz->decryptedID($encrypted_choice_id), 
				'users_id' => Auth::user()->id, 
				'quizzes_id' => $quiz->id
			));
		}
		Completed::create(array(
			'quizzes_id' => $quiz->id, 
			'users_id' => Auth::user()->id
		));
		return Redirect::route('landing')->with('flash-success', '"'.$quiz->title.'" was completed successfully.');
	}
}