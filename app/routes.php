<?php
Route::model('quiz', 'Quiz');
Route::model('user', 'User');

// Landing
Route::get('/', array('uses' => 'LandingController@getIndex', 'as' => 'landing'));
Route::get('login', array('uses' => 'LandingController@getLogin', 'as' => 'login'));
Route::post('login', 'LandingController@postLogin');
Route::get('logout', array('uses' => 'LandingController@getLogout', 'as' => 'logout'));
Route::get('register', array('uses' => 'LandingController@getRegister', 'as' => 'register'));
Route::post('register', 'LandingController@postRegister');
Route::get('account', array('uses' => 'LandingController@getAccount', 'as' => 'account'));
Route::post('account', 'LandingController@postAccount');

// Take Quiz
Route::get('quiz/{quiz}', 'TakeController@getQuiz');
Route::post('quiz/{quiz}', 'TakeController@postQuiz');

// Create Quiz
Route::get('create', array('uses' => 'CreateController@getCreate', 'as' => 'create'));
Route::post('create', 'CreateController@postCreate');
Route::get('addQuestions', array('uses' => 'CreateController@getQuestions', 'as' => 'addQuestions'));
Route::post('addQuestions', 'CreateController@postQuestions');

// Results
Route::get('results', array('uses' => 'ResultsController@getResults', 'as' => 'results'));
Route::post('results', 'ResultsController@postResults');
Route::get('results/{quiz}', 'ResultsController@getResultsQuiz');
Route::post('results/{quiz}', 'ResultsController@postResultsQuiz');
Route::get('results/{quiz}/{user}', 'ResultsController@getResultsUser');

// Manage Quizzes and Users
Route::get('manage', array('uses' => 'ManageController@getManage', 'as' => 'manage'));
Route::post('manage', 'ManageController@postManage');
Route::get('edit/{quiz}', 'ManageController@getEditQuiz');
Route::post('edit/{quiz}', 'ManageController@postEditQuiz');
Route::get('editquestions/{quiz}', 'ManageController@getEditQuestions');
Route::post('editquestions/{quiz}', 'ManageController@postEditQuestions');
Route::get('users', array('uses' => 'ManageController@getUsers', 'as' => 'users'));
Route::post('users', 'ManageController@postUsers');


App::missing(function($exception)
{
	return Redirect::route('landing')->with('flash-error', '404: The requested page wasn\'t found!');
});